# System Stat Monitor
A custom Rainmeter skin in a 3D printed case to show the most important system stats for the CPU, GPU and RAM:
<p align="center">
 <img src="img/1_systemstat.png?raw=true" alt="Skin"/>
</p>
<p align="center">
 <img src="img/1_front.jpg?raw=true" alt="Front"/>
</p>
<p align="center">
 <img src="img/1_side.jpg?raw=true" alt="Side"/>
</p>

# What you need?
 1. Waveshare 7 inch 1024*600 Capacitive Touch Screen LCD Display (https://www.amazon.co.uk/Waveshare-Capacitive-Interface-Raspberry-Beaglebone/dp/B015E8EDYQ)
 2. HDMI cable (included with display)
 3. USB micro cable
 4. Some screws (included with display)

# Software!
 1. You must install Rainmeter (https://www.rainmeter.net/) and HWiNFO (https://www.hwinfo.com/) on your system. After installing, open HWiNFO, go to settings and activate "Shared Memory Support" and "Auto Start".
<p align="center">
 <img src="img/3_hw_settings.png?raw=true" alt="HWINFO settings"/>
</p>

 2. Clone this repo somewhere and copy both skins "ElegantClock" and "SystemStat" folders to your local rainmeter skins folder. If everything was set to default during the installation of Rainmeter, this will be "Documents\Rainmeter\Skins".
 3. Install the HWiNFO_3.2.0.rmskin in the folder HWiNFO by simply double clicking it.
 4. Open Rainmeter, and activate the "LightClock.ini" and "SystemStat.ini" by double clicking it from the list. The "LightClock.ini" does not require any further configuration.

 5. Navigate to "Documents\Rainmeter\Skins\SystemStat\@Ressources" and open the "HWiNFO.inc" file with a text editor. Also launch "HWiNFOSharedMemoryViewer.exe"
 6. You must now put your own sensor ID's in your rainmeter variables file. Therefore put in your own ID's from "HWiNFOSharedMemoryViewer.exe" in your "HWiNFO.inc" file by clicking on the sensor you want to display (1), copy the ID (2) (3) (4) in your file and save it. Do the same for the other variables, but only put the last ID (4) in the missing fields. For the GPU and RAM, repeat (1) (2) (3) (4).
    <p align="center">
     <img src="img/4_sharedmemory.png?raw=true" alt="Shared memory"/>
    </p>

 7. Refresh the skins in Rainmeter and the correct values should be displayed.
 8. Lastly, to change the name of your RAM, simply click on the name and edit the field and the system stat monitor is all set up.

# 3D-Printer!
Following case is used to mount the 7 inch display:
<p align="center">
 <img src="img/2_case.png?raw=true" alt="Shared memory"/>
</p>

You need to print following two models:
  - https://www.thingiverse.com/thing:3743598 (piMac screen)
  - https://www.thingiverse.com/thing:3734539 (piMac foot)

*made with ♥ by jor
